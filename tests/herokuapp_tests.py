import pytest
from urllib import request


@pytest.fixture
def content():
    url = request.urlopen("https://the-internet.herokuapp.com/context_menu")
    web_content = str(url.read())
    return web_content


def test_the_internet_string_appears(content):
    assert content.find(r"Right-click in the box below to see one called \'the-internet\'") != -1


def test_alibaba_string_not_appears(content):
    assert content.find("Alibaba") != -1
